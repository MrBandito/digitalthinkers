﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SelfChechoutMachine.Common
{
    public class CheckoutRequestDto
    {
        
        [JsonProperty("inserted")]
        public StockDto InsertedBillsCoins { get; set; }
        [JsonProperty("price")]
        public int Price { get; set; }
    }
}