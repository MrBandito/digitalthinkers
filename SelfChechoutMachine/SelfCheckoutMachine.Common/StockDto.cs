﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfChechoutMachine.Common
{
 
    public class StockDto
    {
        [JsonProperty("20000")]
        public int TwentyThousands { get; set; }
        [JsonProperty("10000")]
        public int TenThousands { get; set; }
        [JsonProperty("5000")]
        public int FiveThousands { get; set; }
        [JsonProperty("2000")]
        public int TwoThousands { get; set; }
        [JsonProperty("1000")]
        public int OneThousands { get; set; }
        [JsonProperty("500")]
        public int FiveHoundred { get; set; }
        [JsonProperty("200")]
        public int TwoHundred { get; set; }
        [JsonProperty("100")]
        public int OneHundred { get; set; }
        [JsonProperty("50")]
        public int Fifty { get; set; }
        [JsonProperty("20")]
        public int Twnety { get; set; }
        [JsonProperty("10")]
        public int Ten { get; set; }
        [JsonProperty("5")]
        public int Five { get; set; }
    }
}
