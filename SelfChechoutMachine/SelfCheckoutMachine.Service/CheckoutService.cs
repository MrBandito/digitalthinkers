﻿using SelfChechoutMachine.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfCheckoutMachine.Service
{
    public class CheckoutService
    {
        private Dictionary<int, int> availableBillsAndCoins;
        private int[] changesBills = new int[] { 20000, 10000, 5000, 2000, 1000, 500, 200, 100, 50, 20, 10, 5 };

        public CheckoutService()
        {
          
            availableBillsAndCoins = new Dictionary<int, int>();
        }

        public StockDto UpdateStock(Dictionary<int, int> insertedBillsCoins)
        {
            availableBillsAndCoins = insertedBillsCoins;
            return MapTo(insertedBillsCoins);

        }

        public StockDto GetStock()
        {
            return MapTo(availableBillsAndCoins);

        }

        public StockDto Checkout(Dictionary<int, int> insertedBillsCoins,int price)
        {
            var changes = new Dictionary<int, int>();
            var actualCurrency = insertedBillsCoins.Sum(kv => kv.Key * kv.Value);
           var change = actualCurrency - price;
            var isThereAvailableCoins = true;
            foreach (var ch in changesBills)
            {
                var tmp = change / ch;
                if (tmp >0)
                {
                    changes.Add(ch, tmp);
                    change %= ch;
                }
               
            }
            foreach (var coinsBills in changes)
            {
                if (availableBillsAndCoins.TryGetValue(coinsBills.Key,out int amount) && isThereAvailableCoins)
                {
                    if (amount >= coinsBills.Value)
                    {
                        isThereAvailableCoins = isThereAvailableCoins & true;
                        availableBillsAndCoins.Remove(coinsBills.Key);
                        availableBillsAndCoins.Add(coinsBills.Key, amount - coinsBills.Value);
                    }
                    isThereAvailableCoins = isThereAvailableCoins && false;

                }
                else
                {
                    return null;
                }
                
            }
           return MapTo(changes);
        }

        private StockDto MapTo(Dictionary<int,int> coinNumbers)
        {
            var stockDto = new StockDto();
            foreach (var cn in coinNumbers)
            {
                switch (cn.Key)
                {
                    case 20000:
                        stockDto.TwentyThousands = cn.Value;
                        break;
                    case 10000:
                        stockDto.TenThousands = cn.Value;
                        break;
                    case 5000:
                        stockDto.FiveThousands = cn.Value;
                        break;
                    case 2000:
                        stockDto.TwoThousands = cn.Value;
                        break;
                    case 1000:
                        stockDto.OneThousands = cn.Value;
                        break;
                    case 500:
                        stockDto.FiveHoundred = cn.Value;
                        break;
                    case 200:
                        stockDto.TwoHundred = cn.Value;
                        break;
                    case 100:
                        stockDto.OneHundred = cn.Value;
                        break;
                    case 50:
                        stockDto.Fifty = cn.Value;
                        break;
                    case 20:
                        stockDto.Twnety = cn.Value;
                        break;
                    case 10:
                        stockDto.Ten = cn.Value;
                        break;
                    case 5:
                        stockDto.Five = cn.Value;
                        break;
                }
            }

            return stockDto;
        }
    }
}
