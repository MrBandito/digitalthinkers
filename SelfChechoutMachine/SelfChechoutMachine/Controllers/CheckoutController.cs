﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SelfChechoutMachine.Common;
using SelfCheckoutMachine.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SelfChechoutMachine.Controllers
{
    [ApiController]
    [Route("[controller]/api/v1")]
    public class CheckoutController : ControllerBase
    {
        
        private readonly ILogger<CheckoutController> _logger;
        private readonly CheckoutService checkoutService;

        public CheckoutController(ILogger<CheckoutController> logger,CheckoutService checkoutService)
        {
            _logger = logger;
            this.checkoutService = checkoutService;
        }

        [HttpGet]
        [Route("stock")]
        public StockDto GetStock()
        {
            return checkoutService.GetStock();
        }

        [HttpPost]
        [Route("stock")]
        public StockDto SetStock(StockDto stockDto)
        {
            return checkoutService.UpdateStock(MapTo(stockDto));
        }


        [HttpPost]
        [Route("checkout")]
        public IActionResult Checkout([FromBody]CheckoutRequestDto checkoutDto)
        {
            var result = checkoutService.Checkout(MapTo(checkoutDto.InsertedBillsCoins), checkoutDto.Price);
            if (result != null)
            {
                return Ok(result);
            }
            return BadRequest();
        }

        private Dictionary<int, int> MapTo(StockDto stock)
        {
            var coins = new Dictionary<int, int>();
            coins.Add(20000, stock.TwentyThousands);
            coins.Add(10000, stock.TenThousands);
            coins.Add(5000, stock.FiveThousands);
            coins.Add(2000, stock.TwoThousands);
            coins.Add(1000, stock.OneThousands);
            coins.Add(500, stock.FiveHoundred);
            coins.Add(200, stock.TwoHundred);
            coins.Add(100, stock.OneHundred);
            coins.Add(50, stock.Fifty);
            coins.Add(20, stock.Twnety);
            coins.Add(10, stock.Ten);
            coins.Add(5, stock.Five);

            return coins;
        }
    }
}
